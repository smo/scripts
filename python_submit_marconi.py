#/usr/bin/python
import os
import sys
import time
import shutil
from subprocess import call


def generate_script_marconi(file_name,node_number,task_number):
      

          f1=open(file_name,"wb")
          f1.write("#!/bin/bash\n")
#          f1.write("#PBS -q xfuaprod\n")
          f1.write("#PBS -q xfuaprod\n")
          f1.write("#PBS -l walltime=08:55:00\n")
          f1.write("#PBS -l select="+str(node_number)+":ncpus=36:mpiprocs="+str(task_number)+":mem=110GB\n")
          f1.write("#PBS -j oe\n")
          f1.write("#PBS -A FUA10_P_HLST\n")
          f1.write("#PBS -N STARWALL\n")
          f1.write("#PBS -m ae\n")
          f1.write("#PBS -M serhiy.mochalskyy@ipp.mpg.de\n")


          f1.write("cd $PBS_O_WORKDIR\n")
          f1.write("module load intel intelmpi mkl\n")
          f1.write("export I_MPI_DEBUG=5\n")
#          f1.write("export export I_MPI_PIN_PROCESSOR_LIST=0,1\n")
          f1.write("mpirun ./STARWALL_JOREK_Linux<input>my_output\n")
          
          f1.close()
          return

FolderName = time.strftime("%Y_%m_%d_%H_%M")
print FolderName
os.mkdir(FolderName)
         
try:
    os.chdir(FolderName)
except OSError:
    print  "Error: can\'t find folder with name: "+FolderName
else:
    print "Folder " + FolderName + " was created"

binary_exe="../../STARWALL_JOREK_Linux"
assert not os.path.isabs(binary_exe)

input_file="../../input"
assert not os.path.isabs(input_file)

boundary_file="../../boundary.txt"
assert not os.path.isabs(boundary_file)

coil_file="../../coil.txt"
assert not os.path.isabs(coil_file)


mpi_task_number=32

for x in range(1,7):
   
   node_number=2**x
#   if x==7:
#      node_number=7
   file_name="script_"+str(node_number)+"_nodes"

   dir_name=str(node_number)+"_nodes_global_sum";

   os.mkdir(dir_name,0755);
   os.chdir(dir_name);
   shutil.copy(binary_exe,".")
   shutil.copy(input_file,".")  
   shutil.copy(boundary_file,".")
   shutil.copy(coil_file,".")

   generate_script_marconi(file_name,node_number,mpi_task_number)
   call(['qsub', file_name])

   os.chdir("../")


print "all tasks are submitted"


  

